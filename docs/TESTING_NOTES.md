# Notes on Testing Redox

What follows are notes on testing Redox software.

## Types of Builds

Before you can test software, you have to be able to build it. These are different
kinds of build scenarios.

### Local Machines

These builds are driven by the Makefile and configuring your toolchain locally.

To get back to a good state, you should be able to

```
make clean
make pull
make all
```

### Linux Integration Environment

This environment is a Linux environment that is _not_ someone's personal machine.

Things you want to be able to do:

* Build the whole Redox operating system on Linux.
* Cross compile individual Redox programs and libraries on Linux.
* Build Linux compatible programs on Linux.

Gitlab should be sufficient for running these tests:
https://gitlab.redox-os.org/redox-os/redox/pipeline_schedules

### Redox Integration Environment

Here we consider the build context to be a build that is entirely invoked from
Redox. I am actually unclear on the state of builds that run on Redox. It is, at
the very least, less common than building Redox and associated code outside Redox,
whatever the environment.

The bare metal situation is enticing. [Network booting has been achieved](https://www.reddit.com/r/Redox/comments/agugve/redoxos_035_booting_via_pxe_on_old_sony_vaio/), 
and it is an interesting development. And we already know we can boot on 
[lots of stuff](https://www.reddit.com/r/rust/comments/9c94gz/3_wild_redoxen_xpost_rredox/). 
We could imagine a tiny fleet of [mini pcs](https://system76.com/cart/configure/meer4),
perhaps owned by different people, that boot an image that runs builds inside.

There are some complications with the bare metal approach that we have to come
to terms with. My impression is that we currently lack a reliable means of
remote execution into a running Redox environment. We don't have a [remote shell](https://gitlab.redox-os.org/redox-os/redox-ssh/issues/3), 
in particular. Without this feature, it's hard to imagine automation of builds 
running on bare metal Redox at scale. However, we should still design for this 
use case. There are probably many creative solutions, and keeping pxe boot 
working and tested is worth it on its own.

With Redox virtual machines we have more options. We can build many of them on
the same Linux host. We can change our build on the linux host and configure 
our virtual machine to launch in a different way, with different software inside.
We can emulate hardware devices with qemu, and attach them to our Redox vm at
launch. We could perhaps shut down a vm and inspect a qcow2 file for writes and
modifications.

Note that kvm will probably not be available on cloud hosts.

### Packages

If we talk about builds we should talk about packages, because these are the 
bridge builds and tests.

We should invite many implementations, but encourage implementors to draft 
specifications for their packaging systems.

If we are building RPMs or debian packages for linux-compatible utilities, we 
can use a Docker container for the build environment and a Cargo helper to 
build the actual package specs, such as `cargo-deb`.

## Types of Tests

Automating testing requires consistent setup, execution, and inspection of the
results. We can then **assert** that everything went well if what we inspect
matches our expectations.

### Tests Running on Linux

Broad categories of testing Redox stuff on Linux:
* Execute tests for linux-compatible utilities natively on Linux.
* Execute tests against Redox binaries on Linux with the rine emulator.

### Tests Running on Redox

The issues that constrain remote execution of builds also affect remote
execution of tests, and inspection of their results.

Test setup strategies:
* do a redox build with config adjustments to load particular software into a 
  build image. We might consider writing a _test-runner_ as a single program
  for init to start, that loads its config from somewhere and runs tests
* Use `qemu` loader device to [inject binary data directly into RAM](https://github.com/qemu/qemu/blob/master/docs/generic-loader.txt)

Test execution strategies:
* A test-runner is started on boot, and reads configuration for testing 1 or more
  programs. It sets up an env for these programs (by passing params to exec or
  laying down temporary files to disk, etc) and executes them.
* An alternative init could be used on boot, one that only executes tests. This
  has the downside of not running other system daemons, or perhaps letting config
  for those daemons drift

Test inspection strategies:
* _TODO_
* _TODO_

## Types of Software

There are several kinds of software in the project. We can consider the build
and integration test story for each.

### The Kernel

Here we consider the kernel repo and it's dependencies, like the syscall crate. 
The kernel [is not tested on push or schedule](https://gitlab.redox-os.org/redox-os/kernel/pipelines). 


### Drivers

_TODO_

### relibc

To build and test relibc, we need to actually compile Rust and C programs 
against relibc, then test them.

### System Utilities

init, basic networking configs

_TODO_

### Userspace: Unix-compatible

orbtk

_TODO_

### Userspace: Redox-only

_TODO_

### Ion Shell

Ion is special because it is a login shell, and kind of acts as a process
environment for executing other stuff. So we need to test signals and lower-level
IO

_TODO_
