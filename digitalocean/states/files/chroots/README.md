# chroot builds

On modern linux systems, we can achieve simple containerization with a chroot
environment and `systemd-nspawn`.


Test these steps

1. Bootstrap a chroot directory with `mkosi`
1. Copy /srv/pillar and /srv/salt into the chroot's /srv directory
1. Spawn a chroot container with `sudo systemd-nspawn --directory=somedir`
1. Install the salt minion `apt install salt-minion`
1. You're in an Ubuntu chroot! Build Redox! `salt-call --local state.apply`


### mkosi config

The [mkosi utility](https://github.com/systemd/mkosi/blob/master/mkosi.md) is merely 
a python wrapper around standard tools. It's not a daemon, and it doesn't run 
containers. It just builds many kinds of bootable or runnable images. It must be 
run as root.

The following configuration creates a directory that contains an Ubuntu 16.04
operating system file tree.

**mkosi.default**
```ini
[Distribution]
Distribution=ubuntu
Release=xenial
Repositories=main universe multiverse restricted

[Output]
Format=directory
Bootable=no
Hostname=chemist-chroot

[Packages]
WithNetwork=yes
Packages=curl git gcc apt-transport-https sudo
```


