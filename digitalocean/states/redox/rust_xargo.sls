
Install Rust nightly:
  cmd.run:
    - name: "curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain \"nightly\""
    - runas: chemist
    - cwd: /home/chemist
    - env:
      - HOME: "/home/chemist"

# Required to hack the path inside container builds
{% set current_path = salt['environ.get']('PATH', '/bin:/usr/bin') %}

Install Xargo:
  # requires "cc" aka gcc
  cmd.run:
    - name: "cargo install xargo --force"
    - runas: chemist
    - cwd: /home/chemist
    - shell: /bin/bash  # required in container
    - env:
      - HOME: "/home/chemist"
      - PATH: {{ [current_path, '/home/chemist/.cargo/bin']|join(':') }}

