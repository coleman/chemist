
Redox package repositories:
  pkgrepo.managed:
    - humanname: Redox Repos
    - name: deb https://static.redox-os.org/toolchain/apt /
    - file: /etc/apt/sources.list.d/redox.list
    - keyid: AA12E97F0881517F
    - keyserver: keyserver.ubuntu.com

Update repos:
  cmd.run:
    - name: apt update

Install deps:
  pkg.installed:
    - pkgs:
      - cmake
      - make
      - nasm
      - pkg-config
      - libfuse-dev
      - wget
      - gperf
      - libhtml-parser-perl
      - x86-64-unknown-redox-gcc
      - zsh
      - open-iscsi
      - multipath-tools
      - texi2html
      - texinfo
      - autopoint
      - autoconf
      - libtool
      - m4
      - syslinux-utils
      - genisoimage
      - flex
      - bison
      - libpng-dev
      - libhtml-parser-perl
      - jq
      - git
      - qemu
      - mkosi
      - fuse
      - gcc-5-multilib
      - gcc-multilib 
      - lib32asan2 
      - lib32atomic1 
      - lib32cilkrts5 
      - lib32gcc-5-dev 
      - lib32gcc1
      - lib32gomp1
      - lib32itm1
      - lib32mpx0
      - lib32quadmath0
      - lib32stdc++6
      - lib32ubsan0
      - libc6-dev-i386
      - libc6-dev-x32
      - libc6-i386
      - libc6-x32
      - libx32asan2
      - libx32atomic1
      - libx32cilkrts5
      - libx32gcc-5-dev
      - libx32gcc1
      - libx32gomp1
      - libx32itm1
      - libx32quadmath0
      - libx32stdc++6
      - libx32ubsan0
      - gcc-5-multilib
      - gcc-multilib
      - lib32asan2
      - lib32atomic1
      - lib32cilkrts5
      - lib32gcc-5-dev
      - lib32gcc1
      - lib32gomp1
      - lib32itm1
      - lib32mpx0
      - lib32quadmath0
      - lib32stdc++6
      - lib32ubsan0
      - libc6-dev-x32
      - libc6-i386
      - libc6-x32

