
{% set pub_key = pillar["chemist_ssh_public_key"] %}

/home/chemist:
  file.directory:
    - directory: true
    - makedirs: true

Create chemist user:
  user.present:
    - name: chemist
    - home: /home/chemist
    - empty_password: True
    - groups:
      - sudo
      - admin
  ssh_auth.present:
    - name: {{ pub_key }}
    - user: chemist
    - env: ssh-rsa

Change ownership of chemist home:
  cmd.run:
    - name: "chown -R chemist:chemist /home/chemist"

# This effectively runs:
#   sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys AA12E97F0881517F
#   sudo add-apt-repository 'deb https://static.redox-os.org/toolchain/apt /'
include:
  - redox.pkgs

# Required to hack the path inside container builds
{% set current_path = salt['environ.get']('PATH', '/bin:/usr/bin') %}

Install Xargo:
  # requires "cc" aka gcc
  cmd.run:
    - name: "cargo install xargo --force"
    - runas: chemist
    - cwd: /home/chemist
    - shell: /bin/bash  # required in container
    - env:
      - HOME: "/home/chemist"
      - PATH: {{ [current_path, '/home/chemist/.cargo/bin']|join(':') }}


Clone Redox:
  cmd.run:
    - name: git clone https://gitlab.redox-os.org/redox-os/redox.git --origin upstream --recursive
    - runas: chemist
    - cwd: /home/chemist
    - env:
      - HOME: "/home/chemist"
      
Init Redox submodules:
  cmd.run:
    - name: git submodule update --recursive --init
    - runas: chemist
    - cwd: /home/chemist/redox
    - env:
      - HOME: "/home/chemist"

Change ownership of chemist home again:
  cmd.run:
    - name: "chown -R chemist:chemist /home/chemist"


