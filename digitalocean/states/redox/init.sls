
include:
  - redox.user
  - redox.pkgs
  - redox.rust_xargo
  - redox.clone

Drop a qemu launch script:
  file.managed:
    - name: /home/chemist/launch_qemu.sh
    - source: salt://files/launch_qemu.sh
    - user: chemist
    - group: chemist
    - mode: 755
