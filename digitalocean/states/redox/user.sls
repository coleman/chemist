
{% set pub_key = pillar["chemist_ssh_public_key"] %}

/home/chemist:
  file.directory:
    - directory: true
    - makedirs: true

Create chemist user:
  user.present:
    - name: chemist
    - home: /home/chemist
    - empty_password: True
    - groups:
      - sudo
      - admin
  ssh_auth.present:
    - name: {{ pub_key }}
    - user: chemist
    - env: ssh-rsa

Change ownership of chemist home:
  cmd.run:
    - name: "chown -R chemist:chemist /home/chemist"

Passwordless sudo:
  cmd.run:
    - name: "sed -i '/%sudo/ s/%sudo.*/%sudo        ALL=(ALL)       NOPASSWD: ALL/ ' /etc/sudoers"
