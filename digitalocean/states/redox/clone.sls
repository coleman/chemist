
Clone Redox:
  cmd.run:
    - name: git clone https://gitlab.redox-os.org/redox-os/redox.git --origin upstream --recursive
    - runas: chemist
    - cwd: /home/chemist
    - env:
      - HOME: "/home/chemist"
      
# NOTE: using this clone method timed out, so we replace with two
# cmd.run invocations.
# Clone Redox:
#  git.latest:
#    - name: https://gitlab.redox-os.org/redox-os/redox.git
#    - target: /home/chemist/redox
#    - user: chemist
#    - rev: master
#    - branch: master
#    #- origin: upstream   #TODO do we need this?
#    - force_reset: True
#    - submodules: True
      
Init Redox submodules:
  cmd.run:
    - name: git submodule update --recursive --init
    - runas: chemist
    - cwd: /home/chemist/redox
    - env:
      - HOME: "/home/chemist"

Change ownership of chemist home again:
  cmd.run:
    - name: "chown -R chemist:chemist /home/chemist"


