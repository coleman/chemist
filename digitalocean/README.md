# DigitalOcean Packer Build

Build a base image with the code and tools to build Redox.

Requirements:

* packer
* A DigitalOcean account
* A DigitalOcean API key exported as `DIGITALOCEAN_API_KEY`
* An ssh keypair for the `chemist` build user (see below)

### Build the Snapshot Image

Run the build from this directory

```sh
packer build -var ssh_public_key=$HOME/.ssh/some_key.pub base.json
```

This will create a snapshot visible under the images menu in the DigitalOcean
control panel. Snapshots cost $0.05/GB per month. You can use a snapshot to 
launch a live instance.


### Launch a Droplet from the Snapshot

Go to the "images" tab in the DigitalOcean console, and find the latest packer
image. That's your snapshot created after a successful build. You can create a
droplet from it.

Choose a high CPU instance for your Redox build. **These are expensive**, but 
our goal is to only launch them temporarily. **Remember to DESTROY the droplet
when you're done**, or you'll be spending a lot of money.

Once your instance launches, you can ssh in as the `chemist` user with the 
key you provided earlier in the build. The ssh auth has been baked into the
snapshot itself.

```
ssh -i ~/.ssh/$YOUR_KEY chemist@<new droplet ip>
```

From there, look for the **redox** folder in the home directory, and build!

```
cd redox
make all
```

Currently you need to run this once to drop files needed for qemu

```
make qemu kvm=no vga=no
```

You can log in and check out redox at the command line, or you can **exit the
qemu terminal** by pressing _Ctrl+A, X_

After the qemu files are generated, you can launch with the script in the chemist
user home directory

```
cd /home/chemist
./launch_qemu.sh
```

## Build Times

Build from 21 January 2019 on 8GB High CPU Droplet

```
make all  8509.50s user 456.95s system 277% cpu 53:48.52 total
```

## Troubleshooting

If you get an error at the end of your build like this:

```
Build 'digitalocean' errored: Error waiting for snapshot: Get https://api.digitalocean.com/v2/droplets/129452637/actions/613876246: read tcp 192.168.1.151:59439->104.16.24.4:443: read: connection reset by peer

```

Don't despair. Check the cloud console. Your image might actually be there. It's
just a buggy timeout between DigitalOcean and Packer.


