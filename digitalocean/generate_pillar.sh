#!/bin/bash

# This script generates a yaml file for the salt build that contains 
# your ssh public key.  

# The packer flag -var ssh_public_key=/path/to/key is set on the command line,
# and the packer base.json config sets SSH_PUBLIC_KEY in this script's environment.

pub_key=$(cat $SSH_PUBLIC_KEY)

echo "chemist_ssh_public_key: $pub_key" > pillar/chemist/init.sls

