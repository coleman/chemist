# chemist

Just exploring some build configurations. Runs on Linux.

Note: busted and experimental.

## Command line tool dependencies

Run `toolchain_check.ion` to test for the following

* go
* jq
* the packet cli

`$GOPATH/bin` must be on your PATH

## Packet.net setup

You will need a packet.net account. This will probably require a few hours
for confirmation, because new accounts are reviewed by a human.

After you log in, create an Organization and a Project, and note their GUIDs.
Then run

```
./auth.ion
```

And input your default project and api key.

## Configuration

We only manage hosts in a single project. All operations rely on a unique
hostname to identify their targets.

```sh
# chemist.conf
HOST1_USERFILE=cloud-config/one.yml
HOST1_OS=ubuntu_18_04
HOST1_PLAN=baremetal_0
HOST1_FACILITY=sjc1

HOST2_USERFILE=cloud-config/two.yml
HOST2_OS=ubuntu_18_04
HOST2_PLAN=baremetal_0
HOST2_FACILITY=sjc1
```

Since we declare two unique prefixes, HOST1 and HOST2, the following will 
create/ensure two instances are up

```
./compute.ion up
```

## Server deps

On the server itself, we need `jq` and some iSCSI utilities to work with Packet's
block storage devices. See: https://help.packet.net/technical/storage/packet-block-storage-linux

