#!/usr/bin/env bash

#package_name=ion-shell_1.0.0-alpha_amd64.deb
#wget https://coleman.nyc3.digitaloceanspaces.com/software/ion/$package_name
#sudo dpkg -i $package_name

curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain "nightly"
source $HOME/.cargo/env
echo 'source $HOME/.cargo/env' >> $HOME/.bashrc

