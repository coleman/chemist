extern crate bytes;
extern crate futures;
extern crate tempfile;
extern crate tokio;
extern crate tokio_codec;
extern crate tokio_uds;

use bytes::BytesMut;
use futures::{Future, Sink, Stream};
use std::str;
use tokio::io;
use tokio_codec::{Decoder, Encoder};

// NOTE: our domain socket protocol needs to be available to our server code,
// as well, since it's the one that will be talking to the worker.

pub struct ChemistWorkerCodec;

/// A codec to decode datagrams from a unix domain socket as utf-8 text messages.
impl Encoder for ChemistWorkerCodec {
    type Item = String;
    type Error = io::Error;

    fn encode(&mut self, item: Self::Item, dst: &mut BytesMut) -> Result<(), Self::Error> {
        dst.extend_from_slice(&item.into_bytes());
        Ok(())
    }
}

/// A codec to decode datagrams from a unix domain socket as utf-8 text messages.
impl Decoder for ChemistWorkerCodec {
    type Item = String;
    type Error = io::Error;

    fn decode(&mut self, buf: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        let decoded = str::from_utf8(buf)
            .map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e))?
            .to_string();

        Ok(Some(decoded))
    }
}
