#![allow(dead_code)]
#![allow(unused_variables)]

extern crate bytes;
extern crate env_logger;
extern crate futures;
#[macro_use]
extern crate log;
extern crate prost;
#[macro_use]
extern crate prost_derive;
extern crate tokio_core;
extern crate tower_grpc;
extern crate tower_h2;

extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

pub mod chemist_grpc {
    // How do we know this path?
    include!(concat!(env!("OUT_DIR"), "/chemist.rs"));
}

use futures::sync::mpsc;
use futures::{future, Future, Stream};
use tokio_core::net::TcpListener;
use tokio_core::reactor::Core;
use tower_grpc::{Request, Response};
use tower_h2::Server;

use chemist_grpc::{Job, JobRequest, JobStatus, Log};

use std::sync::{Arc, Mutex};

#[derive(Debug, Clone)]
struct Chemist {
    state: Arc<State>,
}

#[derive(Debug)]
struct State {}

impl chemist_grpc::server::Chemist for Chemist {
    //  type GetFeatureFuture = future::FutureResult<Response<Feature>, tower_grpc::Error>;
    type CancelJobFuture = future::FutureResult<Response<JobStatus>, tower_grpc::Error>;
    type EnqueueJobFuture = future::FutureResult<Response<Job>, tower_grpc::Error>;
    // type ListFeaturesStream = Box<Stream<Item = Feature, Error = tower_grpc::Error>>;
    type GetJobLogsStream = Box<Stream<Item = Log, Error = tower_grpc::Error>>;
    // type ListFeaturesFuture = future::FutureResult<Response<Self::ListFeaturesStream>, tower_grpc::Error>;
    type GetJobLogsFuture =
        future::FutureResult<Response<Self::GetJobLogsStream>, tower_grpc::Error>;
    type GetJobStatusFuture = future::FutureResult<Response<JobStatus>, tower_grpc::Error>;

    fn cancel_job(&mut self, request: Request<Job>) -> Self::CancelJobFuture {
        unimplemented!()
    }

    fn enqueue_job(&mut self, request: Request<JobRequest>) -> Self::EnqueueJobFuture {
        future::ok(Response::new(Job {
            id: vec![0, 0, 0, 0xFF],
        }))
    }

    fn get_job_logs(&mut self, request: Request<Job>) -> Self::GetJobLogsFuture {
        unimplemented!()
    }

    fn get_job_status(&mut self, request: Request<Job>) -> Self::GetJobStatusFuture {
        unimplemented!()
    }
}

fn main() {
    let _ = ::env_logger::init();

    let mut core = Core::new().unwrap();
    let reactor = core.handle();
    let c = Chemist {
        state: Arc::new(State {}),
    };
    let new_service = chemist_grpc::server::ChemistServer::new(c);
    // We take a default builder here, but could we continually add to an h2
    // to attach multiple trait implementations to our single tcp listener?
    let h2 = Server::new(new_service, Default::default(), reactor.clone());
    let addr = "127.0.0.1:10000".parse().unwrap();
    let bind = TcpListener::bind(&addr, &reactor).expect("bind");
    println!("listining on {:?}", addr);
    let serve = bind
        .incoming()
        .fold((h2, reactor), |(mut h2, reactor), (sock, _)| {
            if let Err(e) = sock.set_nodelay(true) {
                return Err(e);
            }

            let serve = h2.serve(sock);
            reactor.spawn(serve.map_err(|e| error!("h2 error: {:?}", e)));

            Ok((h2, reactor))
        });

    core.run(serve).unwrap();
}
