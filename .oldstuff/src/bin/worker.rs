extern crate bytes;
extern crate futures;
extern crate tempfile;
extern crate tokio;
extern crate tokio_codec;
extern crate tokio_uds;

use bytes::BytesMut;
use futures::{Future, Sink, Stream};
use std::str;
use tokio::io;
use tokio::runtime::current_thread::Runtime;
use tokio_codec::{Decoder, Encoder};
use tokio_uds::*;

// NOTE: our domain socket protocol needs to be available to our server code,
// as well, since it's the one that will be talking to the worker.

fn main() {}

#[test]
fn framed_echo() {
    use super::*;
    let dir = tempfile::tempdir().unwrap();
    let server_path = dir.path().join("server.sock");
    let client_path = dir.path().join("client.sock");

    let mut rt = Runtime::new().unwrap();

    {
        let socket = UnixDatagram::bind(&server_path).unwrap();
        let server = UnixDatagramFramed::new(socket, ChemistWorkerCodec);

        let (sink, stream) = server.split();

        let echo_stream =
            stream.map(|(msg, addr)| (msg, addr.as_pathname().unwrap().to_path_buf()));

        // spawn echo server
        rt.spawn(
            echo_stream
                .forward(sink)
                .map_err(|e| panic!("err={:?}", e))
                .map(|_| ()),
        );
    }

    // {
    //     let socket = UnixDatagram::bind(&client_path).unwrap();
    //     let client = UnixDatagramFramed::new(socket, StringDatagramCodec);

    //     let (sink, stream) = client.split();

    //     rt.block_on(sink.send(("ECHO".to_string(), server_path)))
    //         .unwrap();

    //     let response = rt.block_on(stream.take(1).collect()).unwrap();
    //     assert_eq!(response[0].0, "ECHO");
    // }
}
