#![allow(dead_code)]
#![allow(unused_variables)]

extern crate bytes;
extern crate env_logger;
extern crate futures;
extern crate http;
extern crate log;
extern crate prost;
#[macro_use]
extern crate prost_derive;
extern crate tokio_core;
extern crate tower_grpc;
extern crate tower_h2;
extern crate tower_http;

extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

use futures::Future;
use tokio_core::net::TcpStream;
use tokio_core::reactor::Core;
use tower_grpc::Request;
use tower_h2::client::Connection;

pub mod chemist_grpc {
    include!(concat!(env!("OUT_DIR"), "/chemist.rs"));
}

use chemist_grpc::*;

pub fn main() {
    let _ = ::env_logger::init();

    let mut core = Core::new().unwrap();
    let reactor = core.handle();

    let addr = "127.0.0.1:10000".parse().unwrap();
    let uri: http::Uri = format!("http://localhost:10000").parse().unwrap();

    let mut client = core
        .run({
            TcpStream::connect(&addr, &reactor)
                .and_then(move |socket| {
                    // Bind the HTTP/2.0 connection
                    Connection::handshake(socket, reactor)
                        .map_err(|_| panic!("failed HTTP/2.0 handshake"))
                })
                .map(move |conn| {
                    use chemist_grpc::client::Chemist;
                    use tower_http::add_origin;

                    let conn = add_origin::Builder::new().uri(uri).build(conn).unwrap();

                    Chemist::new(conn)
                })
        })
        .unwrap();

    let response = core
        .run({ client.enqueue_job(Request::new(JobRequest { config: vec![0] })) })
        .unwrap();

    println!("GetFeature Response = {:?}", response);
}
